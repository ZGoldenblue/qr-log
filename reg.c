



/* example_reg.c - main */

/********************************************************************************/
/*										*/
/* register a service with the inflection server				*/
/*										*/
/* use:   ./example_reg user pass service					*/
/*										*/
/* where user is a valid career account, pass is a random string that is	*/
/*	NOT the user's real password, service is service name			*/
/*										*/
/* The program sends a registration message to the inflection server, and then	*/
/* waits for an access app to connect and send a prompt.  When the prompt	*/
/* arrives, the program sends a reply that says					*/
/*										*/
/*		 "The reg app received the prompt: xxx"				*/
/*										*/
/* where xxx is a copy of the prompt that arrived.  After sending a reply, the	*/
/* reg app closes the TCP connection						*/
/*										*/
/********************************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <time.h>
#include "inflection.h"
#include <sys/wait.h>
#include <time.h>
#include <sqlite3.h> 
#include <pthread.h>

#ifndef	INADDR_NONE
#define	INADDR_NONE	0xffffffff
#endif	/* INADDR_NONE */


#define OPEN  'o'
#define CLOSE 'c'
/* Define buffer size */

#define		BUFF_SIZ	2048	/* size of a buffer for a connection	*/


char status ;

char *command_open[4];

char *command_close[4];




/********************************************************************************/
/*										*/
/* main -- main program for registration app to test the inflection server	*/
/*										*/
/********************************************************************************/
void lock_open(int sock){
  fprintf(stderr,"Lock Open\n");  
  int ret = vfork();

  if(ret==0){
    execvp(command_open[0],command_open);  
    
    exit(0);

  }
  int s;
  waitpid(ret, &s, 0);

  status = OPEN;
  if( sock != 0 )
    send(sock,&status,1,0);
  
}

void lock_close(int sock){

  int ret = vfork();
  fprintf(stderr,"Lock Close\n");
  if(ret==0){
    execvp(command_open[0],command_open);
    _exit(0);

  }
  int s;
  fprintf(stderr,"Lock Close\n");
  waitpid(ret, &s, 0);

  status = CLOSE;
  if(sock != 0 )
    send(sock,&status,1,0);
  
}

void lock_status(int sock){
  send(sock,&status,1,0);
}


void r_int(int sock){
  /* Modified Code*/
  char prompt[20];
  srand ( time(NULL) );
  int r = rand() % 100 + 1;
  char random[20];
  sprintf(random, "%d", r);
  strcpy(prompt,"Random Number is: ");
  strcat(prompt,random);
  send(sock,prompt,strlen(prompt),0);

  /* End Here*/	
}

void date(int sock){
  int ret = vfork();

  if(ret==0){
    dup2(sock,1);
    char *command[2];
    command[0] = strdup("date");
    command[1] = NULL;
    fprintf(stderr,"Sever Side Print in function date\n");
    execvp(command[0],command);
    _exit(0);
  }
  
}


static int callback(void *data, int argc, char **argv, char **azColName){
  int i;
  fprintf(stderr, "%s: ", (const char*)data);
  time_t now;
  for(i=0; i<argc; i++){
    printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    
    time(&now);
    struct tm past;
    strptime(argv[i], "%Y-%m-%d-%H:%M:%S", &past);
    //"2001-11-12 18:31:01"
    double seconds = difftime(mktime(&past),now);
    printf("%.f seconds have passed since the beginning of the month.\n", seconds);   
    
    if( seconds >= 0 ){
      if(status == CLOSE)
	lock_open(0);
      else
	lock_close(0);
    }          
  }

  printf("\n");
  return 0;
}

void *barcode_controller(){
  char buff[255];
  char t ;

  while(1){
    int i = 0;
    while( (t = getchar()) != '\n' ){
      buff[i] = t;
      i++;
    }
    buff[i] = '\0';
    printf("%s\n",buff);


    /*****************************Fetch data from data base***************************************/
    sqlite3 *db;
    char *zErrMsg = 0;
    int  rc;
    char sql[100];
    
    /* Open database */
    rc = sqlite3_open("users.db", &db);
    if( rc ){
      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
      exit(0);
    }else{
      fprintf(stdout, "Opened database successfully\n");
    }

    strcpy(sql,"SELECT DATE from BARID WHERE ID ='");
    strcat(sql,buff);
    strcat(sql,"';");
    fprintf(stderr,"%d(%c): %s\n",strlen(sql),(char)sql[37],sql);
    
    //    sql = "SELECT DATE from BARID WHERE ID = 23653;";


    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
      fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
    }else{
      fprintf(stdout, "Select successfully\n");
    }
    sqlite3_close(db);
  
    //Compare date in data base and current date

  
  }
}


main (int argc, char *argv[]) {

  command_open[0] = strdup("sudo");
  command_open[1] = strdup("python");
  command_open[2] = strdup("motor_control.py");
  command_open[3] = NULL;
  
  command_close[0] = strdup("sudo");
  command_close[1] = strdup("python");
  command_close[2] = strdup("motor_control.py");
  command_close[3] = NULL;

  /*     
   int process_id = fork();
   if(process_id == 0){
     barcode_controller();
   }
  */


  pthread_t thread;
  pthread_create(&thread, NULL, barcode_controller, NULL);


  int		i;			/* loop index			*/
  int		n;			/* number of chars read		*/
  char		*user, *pass, *svc;	/* given by command line args	*/
  char		host[]="xinu00.cs.purdue.edu";	/* location of server	*/
  int		len;			/* string length temporaries	*/

  struct	cmd	*pcmd;			/* ptr to a registration command*/

  struct	hostent	*phe;			/* pointer to host info. entry	*/
  struct	protoent *ppe;			/* ptr. to protocol info. entry	*/
  struct	sockaddr_in socin;		/* an IPv4 endpoint address	*/
  int	addrlen;			/* len of a sockaddr_in struct	*/
	
  int	sock;				/* descriptor for socket	*/

  char	buffer[BUFF_SIZ];		/* input buffer for prompt	*/

  char	reply[] = "This is an echo from the reg side -> ";
  /* reply prefix			*/

  char	replybuf[BUFF_SIZ+sizeof(reply)];	/* reply buffer		*/

  status = OPEN;


  /* check args */

  if (argc != 4) {
    fprintf(stderr, "use is:   ./example_reg user passwd service\n");
    exit(1);
  }

  user = argv[1];
  pass = argv[2];
  svc  = argv[3];

  if (strlen(user) > UID_SIZ) {
    fprintf(stderr, "user name %s is too long\n", user);
    exit(1);
  }

  if (strlen(pass) > PASS_SIZ) {
    fprintf(stderr, "password %s is too long\n", pass);
    exit(1);
  }

  if (strlen(svc) > SVC_SIZ) {
    fprintf(stderr, "Service name %s is too long\n", svc);
    exit(1);
  }

  /* Open socket used to connect to inflection server */

  memset(&socin, 0, sizeof(socin));
  socin.sin_family = AF_INET;

  /* Map host name to IP address or map dotted decimal */

  if ( phe = gethostbyname(host) ) {
    memcpy(&socin.sin_addr, phe->h_addr, phe->h_length);
  } else if ( (socin.sin_addr.s_addr = inet_addr(host)) == INADDR_NONE ) {
    fprintf(stderr, "can't get host entry for %s\n", host);
    exit(1);
  }

  socin.sin_port = htons( (unsigned short)TCPPORT );
  ppe = getprotobyname("tcp");

  /* Create the socket */
  while(1){ 

    sock = socket(PF_INET, SOCK_STREAM, ppe->p_proto);
  if (sock < 0) {
    fprintf(stderr, "cannot create socket\n");
    exit(1);
  }

  /* Connect the socket */

  if (connect(sock, (struct sockaddr *)&socin, sizeof(socin)) < 0) {
    fprintf(stderr, "can't connect to port %d\n", TCPPORT);
    exit(1);
  }
  fprintf(stderr,"Connected with inflection server\n");
  /* Form a registration command and send */

  pcmd = (struct cmd *) buffer;
  pcmd->cmdtype = CMD_REGISTER;

  /* Add user ID */

  len = strlen(user);
  memset(pcmd->cid, ' ', UID_SIZ);
  memcpy(pcmd->cid, user, len);

  pcmd->cslash1 = '/';

  /* Add password */

  len = strlen(pass);
  memset(pcmd->cpass, ' ', PASS_SIZ);
  memcpy(pcmd->cpass, pass, len);

  pcmd->cslash2 = '/';

  /* Add service */

  len = strlen(svc);
  memset(pcmd->csvc, ' ', SVC_SIZ);
  memcpy(pcmd->csvc, svc, len);

  pcmd->dollar  = '$';

  /* Send registration message */
  fprintf(stderr,"Start sending register information!\n");
  send(sock, buffer, sizeof(struct cmd), 0);

  /* Wait for access app to respond by sending data */

  //  n = read(sock, buffer, BUFF_SIZ);
  fprintf(stderr,"Start Reading!\n");
    int buf_idx = 0 ;
    
    while (buf_idx < BUFF_SIZ && 1 == read(sock, &buffer[buf_idx], 1)){
      if (buf_idx > 0  && '\n' == buffer[buf_idx] )
	{
	  fprintf(stderr,"Input length is %d\n",buf_idx);	  
	  break;
	}
      buf_idx++;
    }




    if (buf_idx < 0) {
      fprintf(stderr, "error reading from the socket\n");
      exit(1);
    } else if (buf_idx == 0) {
      fprintf(stderr, "\nTCP connection was closed before a prompt arrived\n");
      exit(0);
    }
  
    /*
      for( i =0; i<BUFF_SIZ;i++){
      if(buffer[i] == '\n' ){
      printf("n is equal to %d\n",i);
      n=i;
      break;
      }
      }
    */
    /* prompt arrived from access app */

  

    buffer[buf_idx] = '\0';
    fprintf(stderr, "\nReceived a prompt: %s\n", buffer);

    /* Send a reply */
    //r_int(sock);
    if( strcmp(buffer,"date") == 0){
      /*
	char* command[2];
	command[0] = strdup("date");
	command[1] = NULL;
	execvp(command[0],command);
      */
      date(sock);
    }else if(strcmp(buffer,"random") == 0){
      r_int(sock);
    }else if(strcmp(buffer,"open") == 0){
      lock_open(sock);
    }else if(strcmp(buffer,"close") == 0){
      lock_close(sock);
    }else if(strcmp(buffer,"status") == 0){
      lock_status(sock);
    }else if(buf_idx >= 4 && buffer[0] =='r' ){
      if( buffer[1] == 'e' && buffer[2] == 'g'){
	
	char* token;
	char* string;
	
	char *sql;
	int t_count = 0;
	sql = strdup("INSERT INTO BARID (ID,DATE) "	\
		     "VALUES ( ");
	
	string = strdup(buffer);
	
	if (string != NULL) {
	  
	
	  
	  while ((token = strsep(&string, ",")) != NULL)
	    {
	      printf("%s\n", token);
	      if( t_count == 2)
		strcat(sql,",'");
	      if( t_count++ > 0)
		strcat(sql,token);
	      
	      
	    }
	  
	
	  
	}

	strcat(sql,"');");
	printf("%s\n",sql);


	sqlite3 *db;
	char *zErrMsg = 0;
	int  rc;
	
	
	/* Open database */
	rc = sqlite3_open("users.db", &db);
	if( rc ){
	  fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	  exit(0);
	}else{
	  fprintf(stdout, "Open database successfully\n");
	}
	
	
	
	fprintf(stdout,"%s\n",sql);
	
	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
	  fprintf(stderr, "SQL error: %s\n", zErrMsg);
	  sqlite3_free(zErrMsg);
	}else{
	  fprintf(stdout, "Insert successfully\n");
	}
	sqlite3_close(db);      
      }
      send(sock,&status,1,0);
    }else{
      char* t = strdup("No such command try date or random\n");
      send(sock,t,strlen(t),0);
    }
  

  /*len = strlen(reply);
    memcpy(replybuf, reply, len);
    memcpy(&replybuf[len], buffer, n);
    send(sock, replybuf, len+n, 0);
  */
  /*
    replybuf[len+n] = '\0';
    fprintf(stderr, "\nSent a reply: %s\n", replybuf);
  */
  fprintf(stderr, "\nClosing the TCP connection.\n");
  close(sock);
  /* int k = 0; */
  /* for(k = 0 ; k <100000 ; k++){ */
  /*   fprintf(stderr,"%d\n",k); */
  /* } */
  }
  
}
