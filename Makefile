#
# Makfile for inflection server examples (registration, access, and clear)
#

all:		acc reg clr

clean:
		rm -f acc reg clr

reg:	reg.c
		gcc -lsqlite3 -lnsl -o reg reg.c

acc:	acc.c
		gcc -lnsl -o acc acc.c

clr:	clr.c
		gcc -lnsl -o clr clr.c
